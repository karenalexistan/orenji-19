firstFn = function() {
let travel = prompt('Did you travel to any countries affected with COVID-19 in the last 14 Days?', 'Enter Yes or No');
	if(travel != null){
		switch(travel.toLowerCase()){
			case "yes":
				symptomsFn();
			break;

			case "no":
				historyFn();
			break;

			default:
				alert("Please enter a valid answer")
				firstFn();
			break;
		}
	}

console.log('Did you have any Travel History? : ' + travel.toUpperCase());

}


symptomsFn = function() {
	let symptoms = prompt('Do you have any symptoms such as Fever (> 38.0°C) AND/OR Respiratory Illness (cough and/or colds)', 'Enter Yes or No');
	if(symptoms != null){
		switch(symptoms.toLowerCase()){
			case "yes":
				puiFn();
			break;

			case "no":
				pumFn();
			break;

			default:
				alert("Please enter a valid answer")
				symptomsFn();
			break;
		}
	}

console.log('Do you have any symptoms? : ' + symptoms.toUpperCase())
}

historyFn = function() {
	let history = prompt('Did you have any History of Exposure? Such as: ' + '\n' + 'A. Providing direct care for COVID-19 patient. ' + '\n' + 'B. Working together or the same close environment of a COVID-19 patient' + '\n' + 'C. Traveling together with COVID-19 patient with any kind of conveyance.' + '\n' + 'D. Living in the same household as a COVID-19 patient within a 14-day period.', 'Enter Yes or No');
	if(history != null){
		switch(history.toLowerCase()){
			case "yes":
				symptomsFn();
			break;

			case "no":
				healthyFn();
			break;

			default:
				alert("Please enter a valid answer")
				historyFn();
			break;
		}
	}

console.log('Did you have any history? : ' + history.toUpperCase())
}

pumFn = function() {
	alert('You are subjected to Person Under Monitoring (PUM) which entails Home Quarantine');
		console.log('Person Under Monitoring (PUM)')
}

puiFn = function() {
	alert('Unfortunately, you are subjected to Patients Under Investigation (PUI). Refer to the FAQs portion on the website to follow the necessary precautions')
		console.log('Patients Under Investigation (PUI)')
}

healthyFn = function() {
	alert('You are healthy! Keep it up and Practice Social Distancing!');
		console.log('You are healthy! Keep it up and practice Social Distancing!');
}

firstFn()



